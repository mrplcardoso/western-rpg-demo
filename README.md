# README #

### What is this repository for? ###

Repository dedicated to keeping the study and implementation 
of mechanics related to games of the Western-RPG genre in general.

### How do I get set up? ###

* Used softwares:


	Unity 2020.2 for Windows

	Visual Studio Community 2019

* Used packages:


	https://assetstore.unity.com/packages/3d/props/food/rpg-food-drinks-pack-121067

	https://assetstore.unity.com/publishers/13852/?orderBy=4&page=1&pageSize=96&plusPro=false&price=0-4000&rating=0&released=0

	https://opengameart.org/content/spell-book-pack-0

	https://opengameart.org/content/cethiels-weapons-3d

	https://opengameart.org/content/tilling-textures-pack-06

	https://assetstore.unity.com/packages/3d/characters/medieval-cartoon-warriors-90079#content

	https://assetstore.unity.com/packages/3d/props/gold-coins-1810#content

### Support Material ###

* Interface:

	https://www.youtube.com/watch?v=6l454lecjs4

* FPS Control:

	https://www.youtube.com/watch?v=_VUpFFVHZVQ

### Who do I talk to? ###

* Repository owner or administrator.

In case of use in other projects, please do not forget 
to give the due credits / source.

# Leia-me #

### Para que serve este repositório? ###

Repositório dedicado a guardar o estudo e implementação 
de mecânicas relacionadas a jogos do gênero RPG Ocidental em geral.

### Como faço para configurar? ###

* Programas usados:


	Unity 2020.2 para Windows

	Visual Studio Community 2019

* Pacotes usados:

	Nenhum

### Com quem eu falo? ###

* Proprietário ou administrador do repositório.

Em caso de uso em outros projetos, por favor não se esqueça
de dar os devidos créditos/fonte.