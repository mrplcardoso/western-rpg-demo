using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
	public string objectName
	{ get; }
	public string interactAction
	{ get; }
	//M�todo que roda quando o jogador
	//olha para um objeto, e deve exibir suas informa��es
	//de intera��o
	public string[] InteractInformation();
	//Roda quando o jogador, ao estiver olhando para o objeto
	//pressiona o bot�o de intera��o
	public void Interact();
}
