using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InteractableDisplay : MonoBehaviour
{
	[SerializeField]
	Image interactionButton;
	[SerializeField]
	TextMeshProUGUI actionName;
	[SerializeField]
	TextMeshProUGUI objectName;

	bool isActive;

	private void Start()
	{
		ChangeActive(false);
	}

	public void ChangeActive(bool set)
	{
		isActive = set;
		interactionButton.gameObject.SetActive(set);
		actionName.gameObject.SetActive(set);
		objectName.gameObject.SetActive(set);
	}

	public void DisplayInteraction(string[] info)
	{
		actionName.text = info[0];
		objectName.text = info[1];
		ChangeActive(true);
	}
}
