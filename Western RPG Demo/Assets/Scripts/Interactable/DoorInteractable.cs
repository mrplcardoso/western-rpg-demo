using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorInteractable : MonoBehaviour, IInteractable
{
	[SerializeField]
	Vector3 openAngle, closeAngle;

	public string objectName
	{ get { return doorName; } }
	[SerializeField]
	string doorName;

	public string interactAction
	{ get { return action; } }
	[SerializeField]
	string action;

	bool isOpen;

	private void Start()
	{
		isOpen = false;
		action = "Abrir";
	}

	public string[] InteractInformation()
	{
		return new string[]{ action, doorName };
	}

	public void Interact()
	{
		StopAllCoroutines();
		//Se o isOpen � verdadeiro a porta esta aberta, e portanto
		//quando o jogador interagiu com uma porta aberta, � pq ele quer
		//fecha-la
		if (isOpen)
		{
			isOpen = false;
			action = "Abrir";
			StartCoroutine(OpenCloseAnimation(transform.eulerAngles, closeAngle, 1));
		}
		else//Caso contr�rio, ele quer abri-la
		{
			isOpen = true;
			action = "Fechar";
			//Ao interagir com a porta, ela deve obrigatoriamente abrir
			//portanto, sai do angulo de fechado, para o angulo de aberto
			StartCoroutine(OpenCloseAnimation(transform.eulerAngles, openAngle, 1));
		}
	}

	//Anima��o que gira a porta para abri-la ou fecha-la
	//em fun��o dos angulos de Euler passados por par�metro
	IEnumerator OpenCloseAnimation(Vector3 start, Vector3 end, float speed)
	{
		float t = 0;
		while (t < 1.01f)
		{
			transform.eulerAngles = Vector3.Lerp(start, end, t);
			t += speed * Time.deltaTime;
			yield return null;
		}
	}
}
