﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryDisplay : MonoBehaviour
{
	//array que guarda todos os elementos filhos do InventoryDisplay
	[SerializeField]
	GameObject[] uiElements;
	public List<ItemIconInvetory> itemIcons
	{ get; private set; }
	[SerializeField]
	ItemIconInvetory itemIcon;
	public List<Image> itemSlots
	{ get; private set; }
	[SerializeField]
	Image itemSlot;

	[SerializeField]
	Vector2 itemGridStart;
	[SerializeField]
	int column, row;

	public bool isActive
	{ get; private set; }

	private void Awake()
	{
		itemIcons = new List<ItemIconInvetory>();
		itemSlots = new List<Image>();
	}

	private void Start()
	{
		//Retorna um array de objetos filhos do InventoryDisplay (elemento de interface)
		//O primeiro item do array é o próprio inventory display, portanto, podemos ignora-lo
		Transform[] elem = GetComponentsInChildren<Transform>();
		//Armazena memória para um array do tamanho do número de elementos filhos
		uiElements = new GameObject[elem.Length-1];
		for (int i = 1; i < elem.Length; ++i)
		{
			uiElements[i-1] = elem[i].gameObject;
		}
		for(int i = 0; i < row; ++i)
		{
			for(int j = 0; j < column; ++j)
			{
				//posição inicial da matriz + (tamanho do elemento * posição no loop)
				Vector2 p = new Vector2(itemGridStart.x + (itemSlot.rectTransform.rect.width * j),
					itemGridStart.y - (itemSlot.rectTransform.rect.height * i));
				itemSlots.Add(Instantiate(itemSlot, p, Quaternion.identity, transform));
				itemIcons.Add(Instantiate(itemIcon, p, Quaternion.identity, transform));
			}
		}
		ChangeActive(false);
	}

	public void ChangeActive(bool set)
	{
		isActive = set;
		for (int i = 0; i < uiElements.Length; ++i)
		{
			uiElements[i].SetActive(set);
		}
		for(int i = 0; i < itemSlots.Count; ++i)
		{
			itemIcons[i].gameObject.SetActive(set);
			if (!set)
			{
				itemIcons[i].iconSprite = null;
				itemIcons[i].item = null;
			}
			itemSlots[i].gameObject.SetActive(set);
		}
	}
}