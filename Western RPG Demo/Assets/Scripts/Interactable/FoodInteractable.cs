using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodInteractable : MonoBehaviour, IInteractable
{
	public string objectName
	{ get { return foodName; } }
	[SerializeField]
	string foodName;

	public string interactAction
	{ get { return action; } }
	[SerializeField]
	string action;

	public string[] InteractInformation()
	{
		return new string[] { action, foodName };
	}

	public void Interact()
	{


	}
}
