using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
	PlayerHub playerHub;
	public float attackRange;
	public float attackRadius;

	Animator animator
	{ get { return playerHub.animator; } }

	public bool drewWeapon
	{ get; private set; }

	private void Start()
	{
		playerHub = GetComponent<PlayerHub>();
		attackRange = 3f;
		attackRadius = 0.5f;

		drewWeapon = false;
		animator.SetBool("drewWeapon", drewWeapon);
	}

	private void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{ TriggerAttack(); }


		SetDrewWeapon();
		Attack();
	}

	void SetDrewWeapon()
	{
		if(Input.GetKeyDown(KeyCode.Q))
		{
			if(drewWeapon)
			{ drewWeapon = false; }
			else
			{ drewWeapon = true; }
			animator.SetBool("drewWeapon", drewWeapon);
		}
	}

	void TriggerAttack()
	{
		RaycastHit hit;
		Physics.SphereCast(playerHub.sceneCamera.transform.position,
			attackRadius, playerHub.sceneCamera.transform.forward,
			out hit, attackRange);

		if(hit.collider != null)
		{
			GameObject g = hit.collider.gameObject;
			Rigidbody b = g.GetComponent<Rigidbody>();
			if(b != null)
			{
				b.AddForce(
					playerHub.sceneCamera.transform.forward * 100);
			}
		}
	}

	void Attack()
	{
		if (Input.GetMouseButtonDown(0))
		{
			drewWeapon = true;
			animator.SetBool("drewWeapon", drewWeapon);
			animator.SetTrigger("attack");
		}
	}
}
