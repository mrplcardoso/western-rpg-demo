using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHub : MonoBehaviour
{
	public PlayerObject playerObject
	{ get; private set; }
	public PlayerMove playerMove
	{ get; private set; }
	public PlayerCamera playerCamera
	{ get; private set; }
	public PlayerInteraction playerInteraction
	{ get; private set; }
	public PlayerInventory playerInventory
	{ get; private set; }
	public PlayerAttack playerAttack
	{ get; private set; }
	public PlayerStats playerStats
	{ get; private set; }

	public CharacterController characterController
	{ get; private set; }
	public Camera sceneCamera
	{ get; private set; }
	public Animator animator
	{ get; private set; }

	private void Awake()
	{
		playerObject = GetComponent<PlayerObject>();
		playerMove = GetComponent<PlayerMove>();
		playerCamera = GetComponent<PlayerCamera>();
		playerInteraction = GetComponent<PlayerInteraction>();
		playerInventory = GetComponent<PlayerInventory>();
		playerAttack = GetComponent<PlayerAttack>();
		playerStats = GetComponent<PlayerStats>();

		characterController = GetComponent<CharacterController>();
		sceneCamera = Camera.main;
		animator = GetComponentInChildren<Animator>();
	}
}
