using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
	PlayerHub playerHub;
	public Dictionary<string, Stat> stats
	{ get; private set; }
	[SerializeField]
	List<Stat> buildStats;

	public void Awake()
	{
		playerHub = GetComponent<PlayerHub>();
		stats = new Dictionary<string, Stat>();
		for(int i = 0; i < buildStats.Count; ++i)
		{
			stats.Add(buildStats[i].name, buildStats[i]);
		}
	}
}
