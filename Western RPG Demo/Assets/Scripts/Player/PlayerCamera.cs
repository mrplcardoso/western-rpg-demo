using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
	PlayerHub playerHub;
	Camera sceneCamera
	{ get { return playerHub.sceneCamera; } }
	[SerializeField]
	Transform cameraPosition;//Definido pelo Inspector

	float xRotation, yRotation;
	[SerializeField]
	float sensitivity;//Definido pelo Inspector
	const float minAngle = -60;
	const float maxAngle = 90;

	private void Awake()
	{
		playerHub = GetComponent<PlayerHub>();
	}

	private void Update()
	{
		RotationInput();
	}

	private void LateUpdate()
	{
		sceneCamera.transform.position = cameraPosition.position;
	}

	void RotationInput()
	{
		xRotation += Input.GetAxisRaw("Mouse X") * sensitivity;
		yRotation += Input.GetAxisRaw("Mouse Y") * sensitivity;
		yRotation = Mathf.Clamp(yRotation, minAngle, maxAngle);

		sceneCamera.transform.eulerAngles =
			new Vector3(-yRotation, xRotation, 0);
		transform.eulerAngles = new Vector3(0, xRotation, 0);
	}
}
