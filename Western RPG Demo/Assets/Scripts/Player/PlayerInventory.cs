using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerInventory : MonoBehaviour
{
	//O dicion�rio abaixo relaciona um tipo de item
	//com uma lista de itens, portanto, ele representa
	//o n�cleo do invent�rio do jogador, pois � ele quem
	//organiza em c�digo os itens e os "bolsos" do invent�rio
	Dictionary<ItemTypes, List<Item>> inventory;
	[SerializeField]
	InventoryDisplay inventoryDisplay;
	public bool isInventoryOpen
	{ get { return inventoryDisplay.isActive; } }

	[SerializeField]
	List<Item> itens;

	private void Start()
	{
		inventory = new Dictionary<ItemTypes, List<Item>>();
		this.ObserveEvent(Notifications.AddToInventory, AddItem);
		this.ObserveEvent(Notifications.PartitionButtonClicked, OnInventoryPartitionClicked);
		this.ObserveEvent(Notifications.OnUseItem, OnUseItem);
		Cursor.lockState = CursorLockMode.Locked;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.I))
		{
			//Se apertou I e o invent�rio esta aberto, o jogador quer fecha-lo
			if (isInventoryOpen)
			{
				Cursor.lockState = CursorLockMode.Locked;
				inventoryDisplay.ChangeActive(false);
			}
			//Se apertou I e o invent�rio esta fechado, o jogador quer abri-lo
			else
			{
				if (inventory.ContainsKey(ItemTypes.Consumable))
				{
					List<Item> l = inventory[ItemTypes.Consumable];
					for (int i = 0; i < l.Count; ++i)
					{
						inventoryDisplay.itemIcons[i].itemPlayerListId = i;
						inventoryDisplay.itemIcons[i].item = l[i];
						inventoryDisplay.itemIcons[i].iconSprite = l[i].sprite;
					}
				}
				inventoryDisplay.ChangeActive(true);
				Cursor.lockState = CursorLockMode.Confined;
			}
		}
	}

	public void AddItem(EventArgs e)
	{
		Item item = (Item)((EventInfo)e).information;
		
		/*Se alguma vez ja foi adicionado no dicion�rio
		 * um item desse tipo, � pq existe uma entrada nele (dicionario)
		 * portanto, ja existe uma lista ali que pode receber esse nome item
		 * ent�o basta acessar esse "bolso" e adicionar o novo item*/
		if(inventory.ContainsKey(item.type))
		{
			inventory[item.type].Add(item);
		}
		else
		{
			/*Quando nenhum item dese tipo foi adicionado, significa que 
			 deve ser feita uma nova entrada de um "bolso", portanto,
			� preciso criar uma nova rela��o tipo-lista*/
			inventory.Add(item.type, new List<Item>());
			inventory[item.type].Add(item);
		}
		itens = inventory[item.type];
	}

	void OnInventoryPartitionClicked(EventArgs e)
	{
		ItemTypes tp = (ItemTypes)((EventInfo)e).information;
		if (inventory.ContainsKey(tp))
		{ this.PostEvent(Notifications.SendInventoryData, new EventInfo(inventory[tp])); }
		else
		{ this.PostEvent(Notifications.SendInventoryData, new EventInfo(null)); }
	}

	void OnUseItem(EventArgs e)
	{
		Tuple<ItemTypes, int> tup = (Tuple<ItemTypes, int>)((EventInfo)e).information;
		inventory[tup.Item1].RemoveAt(tup.Item2);
	}
}
