using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEquipment : MonoBehaviour
{
	//Transform do objeto que carrega a espada
	Transform equipPosition;//m�o do modelo "Heroine"
	public string handTag;//tag adicionada a m�o

	private void Awake()
	{
		equipPosition = 
			GameObject.FindGameObjectWithTag(handTag).transform;
		this.ObserveEvent(Notifications.EquipSword, EquipWeapon);
	}

	void EquipWeapon(EventArgs args)
	{
		//remove espadas equipadas
		equipPosition.DetachChildren();

		GameObject newSword =
			(GameObject)((EventInfo)args).information;
		newSword.transform.parent = equipPosition;
		newSword.transform.localPosition = Vector3.zero;

		newSword.transform.localEulerAngles = new Vector3(-45f,
			newSword.transform.localEulerAngles.y,
			newSword.transform.localEulerAngles.z);
	}
}
