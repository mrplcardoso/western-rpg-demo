using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
	PlayerHub playerHub;
	PlayerStats playerStats
	{ get { return playerHub.playerStats; } }
	CharacterController controller
	{ get { return playerHub.characterController; } }
	Animator animator
	{ get { return playerHub.animator; } }

	Vector3 xVelocity, yVelocity, zVelocity;
	Vector3 velocity 
	{ get { return xVelocity + yVelocity + zVelocity; } }
	
	float speed
	{ get { return playerStats.stats["speed"].value; } }
	float cachedSpeed;
	float jumpForce
	{ get { return playerStats.stats["jump"].value; } } 
	float gravity = 20f;

	private void Awake()
	{
		playerHub = GetComponent<PlayerHub>();
	}

	private void Update()
	{
		MoveInput();
		JumpInput();
		controller.Move(velocity * Time.deltaTime);
	}

	void MoveInput()
	{
		if(Input.GetKey(KeyCode.LeftShift))
		{ cachedSpeed = speed * 2f; }
		else
		{ cachedSpeed = speed; }

		xVelocity = transform.right *
	Input.GetAxisRaw("Horizontal") * cachedSpeed;
		zVelocity = transform.forward *
				Input.GetAxisRaw("Vertical") * cachedSpeed;
		animator.SetFloat("speed", (xVelocity + zVelocity).normalized.magnitude * cachedSpeed);
	}

	void JumpInput()
	{
		animator.SetBool("grounded", controller.isGrounded);
		//Aplica a gravidade no personagem
		yVelocity += Vector3.down * gravity * Time.deltaTime;
		//Se ele estiver no ch�o, n�o � necessario
		//aplicar a gravidade
		if (controller.isGrounded)
		{ yVelocity = Vector3.down;	}
		//Aplica o pulo
		if (controller.isGrounded &&
			Input.GetKeyDown(KeyCode.Space))
		{ 
			yVelocity = Vector3.up * jumpForce;
			animator.SetTrigger("jump");
		}
	}
}
