using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
	PlayerHub playerHub;
	Camera sceneCamera
	{ get { return playerHub.sceneCamera; } }

	[SerializeField]
	InteractableDisplay display;//Definido pelo Inspector

	[SerializeField]
	LayerMask interactionMask;
	[SerializeField]
	float visionDistance;

	private void Awake()
	{
		playerHub = GetComponent<PlayerHub>();
	}

	private void Update()
	{
		RaycastHit hit;//Estrutura que guarda informa��o de colis�o de um Raycast
		Debug.DrawRay(sceneCamera.transform.position, sceneCamera.transform.forward * visionDistance);
		Physics.Raycast(sceneCamera.transform.position, sceneCamera.transform.forward, out hit, visionDistance, interactionMask);
		//Quando o collider da informa��o de colis�o � diferente de nulo
		//significa que o raio colidiu com algum objeto
		if(hit.collider != null)
		{
			IInteractable interactable = hit.collider.GetComponent<IInteractable>();
			if(interactable != null)//Se for diferente de nulo, significa que o objeto � interag�vel
			{
				display.DisplayInteraction(interactable.InteractInformation());
				if(Input.GetKeyDown(KeyCode.E))
				{
					interactable.Interact();
				}
			}
		}
		else
		{ display.ChangeActive(false); }
	}
}
