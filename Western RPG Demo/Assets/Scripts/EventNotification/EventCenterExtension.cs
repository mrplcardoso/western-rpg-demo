using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventCenterExtension
{
	public static void PostEvent(this object sender, string eventName, EventArgs e)
	{
		EventCenter.instance.PostEvent(eventName, e);
	}

	public static void ObserveEvent(this object sender, string eventName, Action<EventArgs> method)
	{
		EventCenter.instance.ObserveEvent(eventName, method);
	}

	public static void RemoveObserver(this object sender, string eventName, Action<EventArgs> method)
	{
		EventCenter.instance.RemoveObserver(eventName, method);
	}
}
