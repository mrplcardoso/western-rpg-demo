using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventCenter
{
	//Dicion�rio que relaciona o nome de um evento com a lista
	//de observadores
	Dictionary<string, List<Action<EventArgs>>> eventDictionary =
		new Dictionary<string, List<Action<EventArgs>>>();

	public static EventCenter instance = new EventCenter();
	private EventCenter() { }

	//� chamado quando um objeto quer passar informa��es para outros
	//Ou notificar terceiros que ele fez algo
	public void PostEvent(string eventName, EventArgs e)
	{
		if(eventDictionary[eventName].Count < 1)
		{ PrintConsole.Log("N�o existem observadores"); return; }

		List<Action<EventArgs>> observers = eventDictionary[eventName];
		for(int i = 0; i < observers.Count; ++i)
		{ observers[i](e); }
	}

	public void ObserveEvent(string eventName, Action<EventArgs> method)
	{
		//Verifica se ja existe uma entrada de observador
		//Caso n�o exista, cria uma lista nova de observadores
		if(!eventDictionary.ContainsKey(eventName))
		{ eventDictionary.Add(eventName, new List<Action<EventArgs>>()); }
		//Impede repetir adi��o de um observador mais de uma vez
		if (eventDictionary[eventName].Contains(method))
		{ PrintConsole.Log("Evento j� observado"); return; }

		eventDictionary[eventName].Add(method);
	}

	public void RemoveEvent(string eventName, Action<EventArgs> method)
	{
		if (!eventDictionary.ContainsKey(eventName))
		{ PrintConsole.Log("Falha na remo��o, evento n�o encontrado"); return; }
		if (!eventDictionary[eventName].Contains(method))
		{ PrintConsole.Log("Falha na remo��o, m�todo n�o encontrado"); return; }
		eventDictionary[eventName].Remove(method);
	}
}
