using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Notifications
{
	public const string LookToInteractable = "LookToInteractable";
	public const string AddToInventory = "AddToInventory";
	public const string PartitionButtonClicked = "PartitionButtonClicked";
	public const string SendInventoryData = "SendInventoryData";
	public const string OnUseItem = "OnUseItem";

	public const string EquipSword = "EquipSword";
}
