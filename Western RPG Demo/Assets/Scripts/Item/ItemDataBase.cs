using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//C�digo que implementa uma simula��o de
//um banco de dados de item
public class ItemDataBase : MonoBehaviour
{
	//Lista de itens existentes no jogo
	//Criada pelo Inspector
	[SerializeField]
	List<Item> itemBase;
	public static List<Item> dataBase
	{ get; private set; }

	private void Start()
	{
		dataBase = itemBase;
	}
}
