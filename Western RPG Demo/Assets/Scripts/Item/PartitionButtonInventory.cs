using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PartitionButtonInventory : MonoBehaviour
{
	public ItemTypes type;
	public List<ItemIconInvetory> iconList;
	Button button;
	Image background;
	Color selected = new Color(255, 200, 0, 150);

	private void Awake()
	{
		button = GetComponent<Button>();
		background = GetComponentsInChildren<Image>()[1];
		this.ObserveEvent(Notifications.SendInventoryData, GetInventoryData);
		this.ObserveEvent(Notifications.PartitionButtonClicked, SetSelection);
	}

	private void Start()
	{
		//Adiciona um evento "OnClick" por c�digo
		//No caso, toda vez que o bot�o for clicado, ele ir� disparar o evento "PartitionButtonClicked"
		button.onClick.AddListener(delegate ()
		{ this.PostEvent(Notifications.PartitionButtonClicked, new EventInfo(type)); });
		iconList = GetComponentInParent<InventoryDisplay>().itemIcons;
	}

	void GetInventoryData(EventArgs e)
	{
		List<Item> l = (List<Item>)((EventInfo)e).information;
		if (l != null)
		{
			int i;
			for (i = 0; i < l.Count; ++i)
			{
				iconList[i].itemPlayerListId = i;
				iconList[i].item = l[i];
				iconList[i].iconSprite = l[i].sprite;
			}
			for (; i < iconList.Count; ++i)
			{
				iconList[i].item = null;
				iconList[i].iconSprite = null;
			}
		}
		else
		{
			for (int i = 0; i < iconList.Count; ++i)
			{
				iconList[i].item = null;
				iconList[i].iconSprite = null;
			}
		}
	}

	void SetSelection(EventArgs e)
	{
		ItemTypes tp = (ItemTypes)((EventInfo)e).information;
		if(tp == type)
		{ background.color = selected; }
		else
		{ background.color = Color.clear; }
	}
}
