using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ItemIconInvetory : MonoBehaviour
{
	public int itemPlayerListId;
	public Item item;//guarda o item que o �cone ira representar
	public Sprite iconSprite//imagem do �cone (que � a imagem do item)
	{ set { iconImage.sprite = value; } }
	Image iconImage;
	Button iconButton;

	private void Awake()
	{
		iconImage = GetComponent<Image>();
		iconButton = GetComponent<Button>();
	}

	private void Start()
	{
		iconButton.onClick.AddListener(delegate ()
		{
			/*Ao clicar no �cone de um item, significa que o jogador quer utilizar esse item
			portanto, rodamos o UseItem do respectivo item, mas, al�m disso, � necess�rio
			retirar o item da visualiza��o do invet�rio (InventoryDisplay) e retirar o item
			do invent�rio do jogador (PlayerInventory).*/
			if (item != null)
			{
				item.prefab.UseItem();
				Tuple<ItemTypes, int> tup = new Tuple<ItemTypes, int>(item.type, itemPlayerListId);
				this.PostEvent(Notifications.OnUseItem, new EventInfo(tup));
				item = null;
				iconSprite = null;
			}
		});
	}
}
