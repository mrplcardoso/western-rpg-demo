using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//Representa um item, exclusivamente, via c�digo
//Serve como uma entrada de item no "banco de dados"
[Serializable]
public class Item
{
	public string itemName;
	public string description;
	public ItemTypes type;
	public ItemInteractable prefab;
	public Sprite sprite;

	public Item ()
	{

	}

	public Item(string name, string descript, ItemTypes itemType, ItemInteractable itemPrefab, Sprite itemSprite)
	{
		itemName = name;
		description = descript;
		type = itemType;
		prefab = itemPrefab;
		sprite = itemSprite;
	}
}
