using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemInteractable : MonoBehaviour, IInteractable
{
	public string objectName
	{ get { return item.itemName; } }
	public Item item;

	public string interactAction
	{ get { return action; } }
	protected readonly string action = "Collect";

	public string[] InteractInformation()
	{
		return new string[] { action, item.itemName };
	}

	public virtual void Interact()
	{
		Item it = new Item(item.itemName, item.description, item.type, item.prefab, item.sprite);
		this.PostEvent(Notifications.AddToInventory, new EventInfo(it));
		Destroy(this.gameObject);
	}

	public abstract void UseItem();
}
