using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemTypes
{
	Consumable, Weapon, MagicBook
}
