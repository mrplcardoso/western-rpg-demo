using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponItem : ItemInteractable
{
	public override void Interact()
	{
		this.PostEvent(Notifications.EquipSword,
			new EventInfo(this.gameObject));
	}

	public override void UseItem()
	{
		
	}
}
