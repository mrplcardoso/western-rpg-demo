using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumableItem : ItemInteractable
{
	public override void UseItem()
	{
		print("Usou o item " + name);
	}
}
