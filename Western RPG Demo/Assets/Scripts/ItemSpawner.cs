using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Classe de teste de itens
public class ItemSpawner : MonoBehaviour
{
	[SerializeField]
	Transform areaCenter;
	float instanceRay = 10f;

	private void Start()
	{
		StartCoroutine(Spawn());
	}

	IEnumerator Spawn()
	{
		while(true)
		{
			yield return new WaitForSeconds(5f);
			
			//Sorteia um item aleatório da base de dados
			int id = Random.Range(0, ItemDataBase.dataBase.Count);
			Item item = ItemDataBase.dataBase[id];

			Vector3 instancePosition = areaCenter.position +
				Random.insideUnitSphere * instanceRay;
			instancePosition.y = 3f;

			ItemInteractable it = Instantiate(item.prefab, instancePosition, Quaternion.identity);
			it.item = item;
		}
	}
}
